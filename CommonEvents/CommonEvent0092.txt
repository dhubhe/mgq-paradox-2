// EVENT 0092
Name "Skill: Play 521-530"

ChangeVariable(11,11,0,2,1,5)
If(1,100,0,522,0)
 If(1,11,0,1,0)
  ShowMessageFace("merlin_fc1",0,0,2,1)
  ShowMessage("【マーリン】")
  ShowMessage("私、少々ながら絵画にも嗜みがありまして……")
  ShowMessageFace("",0,0,2,2)
  ShowMessage("マーリンは絵を描いている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("merlin_fc1",0,0,2,3)
  ShowMessage("【マーリン】")
  ShowMessage("思えば、遠くまで来たものですね……")
  ShowMessageFace("",0,0,2,4)
  ShowMessage("マーリンは物思いに浸っている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("merlin_fc1",0,0,2,5)
  ShowMessage("【マーリン】")
  ShowMessage("はっ！")
  355("interrupt_skill(3316)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("merlin_fc1",0,0,2,6)
  ShowMessage("【マーリン】")
  ShowMessage("おやつを用意しました……")
  ShowMessageFace("",0,0,2,7)
  ShowMessage("マーリンからの差し入れだ！")
  250(bytes(0x04,0x08,0x6f,0x3a,0x0c,0x52,0x50,0x47,0x3a,0x3a,0x53,0x45,0x08,0x3a,0x0a,0x40,0x6e,0x61,0x6d,0x65,0x49,0x22,0x0a,0x49,0x74,0x65,0x6d,0x31,0x06,0x3a,0x06,0x45,0x54,0x3a,0x0b,0x40,0x70,0x69,0x74,0x63,0x68,0x69,0x69,0x3a,0x0c,0x40,0x76,0x6f,0x6c,0x75,0x6d,0x65,0x69,0x55))
  ChangeInventory_Item(423,0,0,1)
  ShowMessageFace("",0,0,2,8)
  ShowMessage("「プリン」を手に入れた！")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("merlin_fc1",0,0,2,9)
  ShowMessage("【マーリン】")
  ShowMessage("私、少々ながら音楽にも嗜みがありまして……")
  355("interrupt_skill(3305)")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
If(1,100,0,525,0)
 ChangeVariable(11,11,0,2,1,10)
 If(1,11,0,1,0)
  ShowMessageFace("sonia_fc1",0,0,2,10)
  ShowMessage("\\n<Sonya>I wonder how uncle Lazarus is doing?\nI should write him a letter.")
  ShowMessageFace("",0,0,2,11)
  ShowMessage("Sonya begins to write a letter.")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("sonia_fc2",5,0,2,12)
  ShowMessage("\\n<Sonya>Sei!　Deyaa!!")
  ShowMessageFace("",0,0,2,13)
  ShowMessage("Sonia is practicing swinging her weapon...")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("sonia_fc2",5,0,2,14)
  ShowMessage("\\n<Sonya>Sei!　Deyaa!!")
  355("interrupt_skill(3291)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("sonia_fc2",5,0,2,15)
  ShowMessage("\\n<Sonya>Doyaaaaa!!")
  355("interrupt_skill(3292)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("sonia_fc2",0,0,2,16)
  ShowMessage("\\n<Sonya>Stop it, all of you!\nIt's not a competition for me!")
  ShowMessageFace("",0,0,2,17)
  ShowMessage("Sonya tries to stop the fight!")
  ShowMessage("...but no one listened.")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,6,0)
  ShowMessageFace("sonia_fc1",1,0,2,18)
  ShowMessage("\\n<Sonya>A maiden's smile...")
  ShowMessageFace("",0,0,2,19)
  ShowMessage("Sonya smiled!")
  ShowMessage("...but nothing happened.")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,7,0)
  ShowMessageFace("sonia_fc2",3,0,2,20)
  ShowMessage("\\n<Sonya>Hmmm, I wonder if my breasts have gotten bigger?")
  ShowMessageFace("sonia_fc1",4,0,2,21)
  ShowMessage("\\n<Sonya>...*glance*.")
  ShowMessageFace("",0,0,2,22)
  ShowMessage("Sonya was trying to appeal to somebody...")
  ShowMessage("But no one was looking!")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,8,0)
  ShowMessageFace("sonia_fc3",4,0,2,23)
  ShowMessage("\\n<Sonya>...")
  ShowMessageFace("",0,0,2,24)
  ShowMessage("Sonya gave a sidelong glance...")
  ShowMessage("But it looked more like she was staring.")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,9,0)
  ShowMessageFace("sonia_fc1",1,0,2,25)
  ShowMessage("\\n<Sonya>This is... a mother's warmth...?")
  355("interrupt_skill(3302)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,10,0)
  ShowMessageFace("sonia_fc3",2,0,2,26)
  ShowMessage("\\n<Sonya> ________________\n > Supernatural Phenomenon <\n Y^Y^Y^Y^Y^Y^Y")
  313(0,525,0,12)
  ShowMessageFace("",0,0,2,27)
  ShowMessage("Sonya was paralyzed!")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
If(1,100,0,529,0)
 If(1,11,0,1,0)
  ShowMessageFace("sara_fc1",1,0,2,28)
  ShowMessage("\\n<Sara>Even I am a princess.\nI have had many lessons since I was little...")
  ShowMessageFace("",0,0,2,29)
  ShowMessage("Sara began a strange dance!")
  ShowMessage("...an awkward silence followed.")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("sara_fc1",0,0,2,30)
  ShowMessage("\\n<Sara>Even if just a bit, I need to practice my sword skills!")
  355("interrupt_skill(3291)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("sara_fc1",1,0,2,31)
  ShowMessage("\\n<Sara>Fufuu, shall we have some fun...?")
  ShowMessageFace("",0,0,2,32)
  ShowMessage("Sara is working on her catchphrase..")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("sara_fc1",2,0,2,33)
  ShowMessage("\\n<Sara>In the name of the Queen of Sabasa, I command you!\nBoth sides, lay down your arms!")
  ShowMessageFace("",0,0,2,34)
  ShowMessage("Sara tried to stop the fight!")
  ShowMessage("...but no one listened.")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("sara_fc1",1,0,2,35)
  ShowMessage("\\n<Sara>Even I am a princess.\nI have had many lessons since I was little...")
  355("interrupt_skill(3305)")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
If(1,100,0,530,0)
 If(1,11,0,1,0)
  ShowMessageFace("sara_fc4",0,0,2,36)
  ShowMessage("【サラ】")
  ShowMessage("うふふっ……")
  ShowMessageFace("",0,0,2,37)
  ShowMessage("サラはくすくす笑っている……")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,2,0)
  ShowMessageFace("sara_fc4",0,0,2,38)
  ShowMessage("【サラ】")
  ShowMessage("うふふっ……")
  355("interrupt_skill(3296)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,3,0)
  ShowMessageFace("sara_fc4",0,0,2,39)
  ShowMessage("【サラ】")
  ShowMessage("……………………")
  313(0,530,0,24)
  ShowMessageFace("",0,0,2,40)
  ShowMessage("サラはいやらしい事を考えている……")
  ShowMessage("サラは敏感になってしまった！")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,4,0)
  ShowMessageFace("sara_fc4",0,0,2,41)
  ShowMessage("【サラ】")
  ShowMessage("あなた、可愛いわね……")
  355("interrupt_skill(3289)")
  EndEventProcessing()
  0()
 EndIf()
 If(1,11,0,5,0)
  ShowMessageFace("sara_fc4",0,0,2,42)
  ShowMessage("【サラ】")
  ShowMessage("うふふっ……")
  355("interrupt_skill(3290)")
  EndEventProcessing()
  0()
 EndIf()
 0()
EndIf()
0()
